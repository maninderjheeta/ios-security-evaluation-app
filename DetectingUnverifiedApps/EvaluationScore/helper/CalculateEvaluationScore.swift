//
//  CalculateEvaluationScore.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/14/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
//This is a helper method for calculating the evaluation score for both actual and simulation screen.

//import Foundation
import UIKit
import SystemConfiguration.CaptiveNetwork
import LocalAuthentication

class EvaluationScoreCalcualtions {
    
    func calculateScore(selectedParameters parameters: [Int], completioinHandler: @escaping (Int, Int) -> ()) {
        for parameter in parameters {
            if(parameter == 0) {
                //iOS version
                iOSVersionEvaluationCalculations().calculateiOSEvluationScore(completionHandler: completioinHandler)
            }
            if(parameter == 1) {
                //Authentication Evaluation
                AuthenticationEvaluationCalculation().calculateAuthenticationEvaluationScore(completionHandler: completioinHandler)
            }
            if (parameter == 2) {
                //Jailbroken Evaluation
                JailBrokenEaluationCalculation().calculateJailBrokenEvalautionScore(completionHandler: completioinHandler)
            }
            if (parameter == 3) {
                //Unverified Evaluation
                UnverifiedAppsEvaluationScoreCalcualtion().calculateUnverifiedAppsEvaluationScore(completionHandler: completioinHandler)
            }
        }
    }
    
    func calculateScoreSimulation(selectedParameters parameters: [Int], completioinHandler: @escaping (Int, Int) -> ()) {
        let evaluationScore = 1
        for parameterId in parameters {
            if(parameterId == 0) {
                //iOS version
                completioinHandler(evaluationScore, parameterId)
            }
            if(parameterId == 1) {
                //Authentication Evaluation
                completioinHandler(evaluationScore, parameterId)
            }
            if (parameterId == 2) {
                //Jailbroken Evaluation
                completioinHandler(evaluationScore, parameterId)
            }
            if (parameterId == 3) {
                //Unverified Evaluation
               completioinHandler(evaluationScore, parameterId)
            }
        }
    }
}
