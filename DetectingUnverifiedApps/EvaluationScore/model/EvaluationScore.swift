//
//  EvaluationScore.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/15/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
// Model for representing the evaluation parameter score

import Foundation

class EvaluationScore {
    
    var iOSVersionEval: Int
    var iOSVersionEvalGetSet: Int {
        get {
            return self.iOSVersionEval
        }
        set {
            self.iOSVersionEval = newValue
        }
    }
    
    var authenticationEval: Int
    var authenticationEvalGetSet: Int {
        get {
            return self.authenticationEval
        }
        set {
            self.authenticationEval = newValue
        }
    }
    
    var jailbrokenEval: Int
    var jailbrokenEvalGetSet: Int {
        get {
            return self.jailbrokenEval
        }
        set {
            self.jailbrokenEval = newValue
        }
    }
    
    var unverifiedAppEval: Int
    var unverifiedAppEvalGetSet: Int {
        get {
            return self.unverifiedAppEval
        }
        set {
            self.unverifiedAppEval = newValue
        }
    }
    
    
    init() {
        self.iOSVersionEval = -1
        self.authenticationEval = -1
        self.jailbrokenEval = -1
        self.unverifiedAppEval = -1
    }
    
    func setEvaluationScoreForEachParameter(evaluationScore score: Int, evaluationParameterId parameterId: Int) {
        switch parameterId {
        case 0:
            //ios
            self.iOSVersionEval = score
        case 1:
            //authentication
            self.authenticationEval = score
        case 2:
            //jailbroken
            self.jailbrokenEval = score
        case 3:
            //unverified
            self.unverifiedAppEval = score
        default:
            print("Not matching any parameter")
        }
    }
    
    func calculateEvalautionScore() -> String {
        return String(self.iOSVersionEval + self.authenticationEval + self.jailbrokenEval + self.unverifiedAppEval)
    }
    
    func getParemetersEvalautionScore() -> [String: Int] {
        var parametersEvalautionScore: [String: Int]  = [:]
        if(self.iOSVersionEval != -1) {
            parametersEvalautionScore["iOSVersionEval"] = self.iOSVersionEval
        }
        if(self.authenticationEval != -1) {
            parametersEvalautionScore["authenticationEval"] = self.authenticationEval
        }
        if(self.jailbrokenEval != -1) {
            parametersEvalautionScore["jailbrokenEval"] = self.jailbrokenEval
        }
        if(self.unverifiedAppEval != -1) {
            parametersEvalautionScore["unverifiedAppEval"] = self.unverifiedAppEval
        }
        
        return parametersEvalautionScore
    }
    
    static func getParamterReadableTitle(parameterKey key: String) -> String {
        var title: String = ""
        switch key {
        case "iOSVersionEval":
            //ios
            title = "iOS Version Evalaution"
        case "authenticationEval":
            //authentication
            title = "Authentication Evaluation"
        case "jailbrokenEval":
            //jailbroken
            title = "Jail Broken Evaluation"
        case "unverifiedAppEval":
            //unverified
            title = "Unverified App Evaluation"
        default:
            print("Not matching any parameter")
        }
        
        return title
    }

    static func getImproveEvalInformation(parameterReadableTitle title: String) -> String {
        var info: String = ""
        switch title {
        case "iOS Version Evalaution":
            //ios
            info = "iOS Version Evalaution can be improved by installing the latest iOS version. Also, install the latest updates so that device is not vulnerable to security attacks."
        case "Authentication Evaluation":
            //authentication
            info = "Using Screen Lock is very important for the security of your data. A device not having an authentication mechanism is more prone to data leaks. Attacker can physical get hold of you device and will be able to retrieve all your personal information without having to go through any struggles of cracking the encryption which is very difficult."
        case "Jail Broken Evaluation":
            //jailbroken
            info = "Your device has been Jailbroken. According to apple if a device is jailbroken by the user its warranty goes void. A jailbroken device is more prone to attacks by an unauthorized application because the application no more runs in sandbox. They can get access root level resources which could make the device vulnerable to cyberattacks."
        case "Unverified App Evaluation":
            //unverified
            info = "Your device has unauthroized apps installed on your phone. These apps has not been authroized by apple as they are not avaialable on App store. Installing unauthorized apps on your device can have severe consequences. These apps can retrieve your personal data such as images, files, financial informaiton etc. It is recommended to install apps avaialble on App store as they have been passed by Apple after reviewing their code and privacy policies."
        default:
            print("Not matching any parameter")
        }
        
        return info
    }
}
