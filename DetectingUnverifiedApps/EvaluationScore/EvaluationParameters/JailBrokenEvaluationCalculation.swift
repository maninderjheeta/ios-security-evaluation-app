//
//  JailbrokenEvaluationCalculation.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/22/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
// This class calculate the score for Jailbroken Evaluation parameter.
//Credit goes to a Stack overflow answer:
//Reference: https://stackoverflow.com/questions/413242/how-do-i-detect-that-an-ios-app-is-running-on-a-jailbroken-phone

import Foundation
import UIKit

class JailBrokenEaluationCalculation {
    let parameterId = 2
    
    func calculateJailBrokenEvalautionScore(completionHandler: @escaping (Int, Int) -> ()) {
        var jailbrokenEvaluationScore = 0
        if(!isJailBroken()) {
            jailbrokenEvaluationScore = 1
        }
        
        completionHandler(jailbrokenEvaluationScore, self.parameterId)
    }
    
    func isJailBroken() -> Bool {
        guard let cydiaUrlScheme = NSURL(string: "cydia://package/com.example.package") else { return isJailBrokenHelper() }
        return UIApplication.shared.canOpenURL(cydiaUrlScheme as URL) || isJailBrokenHelper()
    }
    
    func isJailBrokenHelper() -> Bool {
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: "/Applications/Cydia.app") ||
            fileManager.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib") ||
            fileManager.fileExists(atPath: "/bin/bash") ||
            fileManager.fileExists(atPath: "/usr/sbin/sshd") ||
            fileManager.fileExists(atPath: "/etc/apt") ||
            fileManager.fileExists(atPath: "/usr/bin/ssh") {
            return true
        }
        
        if canOpen(path: "/Applications/Cydia.app") ||
            canOpen(path: "/Library/MobileSubstrate/MobileSubstrate.dylib") ||
            canOpen(path: "/bin/bash") ||
            canOpen(path: "/usr/sbin/sshd") ||
            canOpen(path: "/etc/apt") ||
            canOpen(path: "/usr/bin/ssh") {
            return true
        }
        
        let path = "/private/" + NSUUID().uuidString
        do {
            try "anyString".write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
            try fileManager.removeItem(atPath: path)
            return true
        } catch {
            return false
        }
    }
    
    func canOpen(path: String) -> Bool {
        let file = fopen(path, "r")
        guard file != nil else { return false }
        fclose(file)
        return true
    }
}
