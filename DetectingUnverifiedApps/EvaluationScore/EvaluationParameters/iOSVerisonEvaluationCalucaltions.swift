//
//  iOSVerisonEvaluationCalucaltions.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/21/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
// This class calculate the score for iOS version Evaluation parameter.

import Foundation
import UIKit
import Firebase

class iOSVersionEvaluationCalculations {
    let parameterId = 0
    
    func calculateiOSEvluationScore(completionHandler: @escaping (Int, Int) -> ()) {
        let uidUser = Auth.auth().currentUser!.uid
        self.iOSEvluationScoreCalculationHelper(userUID: uidUser, completionHandler: completionHandler)
    }
    
    public func iOSEvluationScoreCalculationHelper(userUID uid : String,  completionHandler: @escaping (Int, Int) -> ()) {
        var isVersionEvaluationScore: Int = 0
        if(uid.isEmpty) {
            print("uid is empty")
            isVersionEvaluationScore = 0
            completionHandler(isVersionEvaluationScore, self.parameterId)
            return
        }
        
        let deviceCurrentiOSVersion = getSystemVersion()
        let deviceModel = String(getDeviceModel().split(separator: ",")[0]).lowercased()
        
        let ref = Database.database().reference()
        
        let childKey = "\(uid)/LatestiOSVersions/\(deviceModel)"
        print("Key: \(childKey)")
        ref.child(childKey).observeSingleEvent(of: .value) { (snapshot) in
            if(snapshot.value as? String == deviceCurrentiOSVersion) {
                isVersionEvaluationScore = 1
            }
            
            completionHandler(isVersionEvaluationScore, self.parameterId)
        }
    }
    
    func getDeviceModel() -> String {
        let modelString = Sysctl.model
        return modelString
    }
    
    func getiOSBuildVersion() -> String {
        let osVersionString = Sysctl.osVersion
        return osVersionString
    }
    
    func getSystemVersion() -> String {
        let systemVersion = UIDevice.current.systemVersion
        return systemVersion
    }
}
