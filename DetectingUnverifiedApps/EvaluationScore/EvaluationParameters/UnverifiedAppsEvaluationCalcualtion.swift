//
//  UnverifiedAppsEvaluationCalcualtion.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/22/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
// This class calculate the score for Unverified App Evaluation parameter.
//It checks to see if the unverified app are present in the Firebase realtime database.

import Foundation
import Firebase

class UnverifiedAppsEvaluationScoreCalcualtion {
    let parameterId = 3
    
    func calculateUnverifiedAppsEvaluationScore(completionHandler: @escaping (Int, Int) -> ()) {
        var unverifiedAppsEvaluationScore = 0
        let uidUser = Auth.auth().currentUser!.uid
        let ref = Database.database().reference()
        
        let childKey = "\(uidUser)/appLabels/unverifiedAppLabels"
        ref.child(childKey).observeSingleEvent(of: .value) { (snapshot) in
            if !snapshot.exists() {
                unverifiedAppsEvaluationScore = 1
            }
            
            completionHandler(unverifiedAppsEvaluationScore, self.parameterId)
        }
    }
}
