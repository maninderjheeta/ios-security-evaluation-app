//
//  AuthenticationEvalautionCalcualtion.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/22/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
// This class calculate the score for Authenticaiton Evaluation parameter.

import Foundation
import LocalAuthentication

class AuthenticationEvaluationCalculation {
    let parameterId = 1
    
    func calculateAuthenticationEvaluationScore(completionHandler: @escaping (Int, Int) -> ()) {
        var authenticationEvaluationScore = 0
        if(devicePasscodeSet()) {
            authenticationEvaluationScore = 1
        }
        
        completionHandler(authenticationEvaluationScore, self.parameterId)
    }
    
    public func devicePasscodeSet() -> Bool {
//        checks to see if devices (not apps) passcode has been set.
//        No need to check Biometeric as passcode is required in either case
        return LAContext().canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
    }
    
    func biometricType() -> BiometricType {
        let authContext = LAContext()
        if #available(iOS 11, *) {
            let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            switch(authContext.biometryType) {
            case .none:
                return .none
            case .touchID:
                return .touch
            case .faceID:
                return .face
            default:
                return .none
            }
        } else {
            return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touch : .none
        }
    }
    
    enum BiometricType {
        case none
        case touch
        case face
    }
    
}
