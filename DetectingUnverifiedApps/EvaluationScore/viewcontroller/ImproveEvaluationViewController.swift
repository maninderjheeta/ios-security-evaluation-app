//
//  ImproveEvaluationViewController.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/24/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
//This is the view controller for the showing the Improvement text for each evaluation parameter.

import UIKit

class ImproveEvaluationViewController: UIViewController {
    var reportVC: ReportTableViewController?
    var evaluaitionReadableTitle: String = ""
    
    @IBOutlet weak var moreInfoParameterText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.moreInfoParameterText.text =  EvaluationScore.getImproveEvalInformation(parameterReadableTitle: self.evaluaitionReadableTitle)
        
        self.moreInfoParameterText.sizeToFit()
    }
}
