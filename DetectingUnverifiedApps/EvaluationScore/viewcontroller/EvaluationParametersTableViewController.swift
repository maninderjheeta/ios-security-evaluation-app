//
//  EvaluationParametersTableViewController.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/14/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
//This is table view controller for the static Evaluation Parameters

import UIKit
import Firebase

class EvaluationParametersTableViewController: UITableViewController {
    
    var selectedEvaluationParameters: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        displayEvaluationParameterInfo(rowNum: indexPath.row)
    }
    
    func displayEvaluationParameterInfo(rowNum row: Int) {
        switch row {
            case 0:
                displayiOSVersionEvaluation()
            case 1:
                displayAuthenticationEvaluation()
            case 2:
                displayJailBrokenEvaluation()
            case 3:
                displayUnverifiedAppsEvaluation()
            default:
                print("Not possible")
        }
    }
    
    func displayiOSVersionEvaluation() {
        let title = "iOS version Evaluation"
        let message = "This evaluates if the device has the latest verison of iOS"
        alertForEvaluationInfo(title: title, message: message)
    }
    
    func displayAuthenticationEvaluation() {
        let title = "Authentication Evalaution"
        let message = "This evaluates if the device has any authenticaiton mechanism such as password or biometerics"
        alertForEvaluationInfo(title: title, message: message)
    }
    
    func displayJailBrokenEvaluation() {
        let title = "Jailbroken Evaluation"
        let message = "This evaluates if the device is Jailbroken or not"
        alertForEvaluationInfo(title: title, message: message)
    }
    
    func displayUnverifiedAppsEvaluation() {
        let title = "Unverfied Apps Evaluation"
        let message = "This evaluates if the device has any unverified installed on the device or not"
        alertForEvaluationInfo(title: title, message: message)
    }
    
    func alertForEvaluationInfo(title evalTitle: String, message evalMessage: String) {
        let alert = UIAlertController(title: evalTitle, message: evalMessage, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Call Evaluations here
        let index = indexPath.row
        if(index == 3) {
            self.handleAppDetecetionHasBeenRun(indexPath: indexPath)
        } else {
            self.selectedEvaluationParameters.append(index)
        }
    }
    
    func handleAppDetecetionHasBeenRun(indexPath: IndexPath) {
        let uidUser = Auth.auth().currentUser!.uid
        let ref = Database.database().reference()
        
        let childKey = "\(uidUser)/appLabels"
        ref.child(childKey).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                self.selectedEvaluationParameters.append(indexPath.row)
            } else {
                self.tableView.deselectRow(at: indexPath, animated: true)
                self.presentAlertAppsNotDetected()
            }
        }
    }
    
    func presentAlertAppsNotDetected() {
        let title = "Apps Not Detected Yet"
        let message = "Please go to the Unverified Apps Screen. It provides option to deteect unverified apps. Without this we cannot evaluate this parameter. Sorry for the inconvenience caused."
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Go to Detect Unverified App Screen", style: .default, handler: {(action: UIAlertAction) in
                if let detectUnverifiedAppVC = self.storyboard?.instantiateViewController(withIdentifier: "detectUnverifiedAppsVC") as? DetectUnverifiedAppsViewController {
                    self.navigationController?.pushViewController(detectUnverifiedAppVC, animated: true)
                }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        removeParameterSelectedParameters(with: indexPath)
    }
    
    func removeParameterSelectedParameters(with indexPath: IndexPath) {
        if let index = self.selectedEvaluationParameters.firstIndex(of: indexPath.row) {
            self.selectedEvaluationParameters.remove(at: index)
        }
    }
}
