//
//  ReportTableViewController.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/24/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
//This is view controller for the Report Screen

import UIKit

class ReportTableViewController: UITableViewController {
    var parametersEvalautionScore: [String: Int] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return parametersEvalautionScore.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "parameterInfoCell", for: indexPath)

        // Configure the cell...
        if let firstParameterKey = parametersEvalautionScore.first?.key {
            let scoreFirstParameterScore = parametersEvalautionScore[firstParameterKey]!
            
            parametersEvalautionScore.removeValue(forKey: firstParameterKey)
            
            cell.textLabel?.text = EvaluationScore.getParamterReadableTitle(parameterKey: firstParameterKey)
            cell.detailTextLabel?.text = "Score: \(scoreFirstParameterScore)"
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let improveEvalScoreVC = storyboard?.instantiateViewController(withIdentifier: "improveEvaluationScore") as? ImproveEvaluationViewController {
            let indexPath = self.tableView.indexPathForSelectedRow
            let currentCell = self.tableView.cellForRow(at: indexPath!)
            
            improveEvalScoreVC.reportVC = self
            improveEvalScoreVC.evaluaitionReadableTitle = currentCell?.textLabel?.text ?? ""
            
            navigationController?.pushViewController(improveEvalScoreVC, animated: true)
        }
    }
}
