//
//  SimulationEvaluationScoreViewController.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/24/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
//This is View controller for the simulation screen

import UIKit

class SimulationEvaluationViewController: UIViewController {
    var childVC: EvaluationParametersTableViewController?
    var evaluationScore = EvaluationScore()
    
    @IBOutlet weak var viewReport: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //view report ramains disabled untill user evalaute score
        disableViewReportButton()
        childVC = self.children.first as? EvaluationParametersTableViewController
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBAction func viewEvaluationReport(_ sender: Any) {
        //Open a modal showing information on how user can improve their evaluation score
        //Pass the evaluationScore object
        self.performSegue(withIdentifier: "simulationToReport", sender: self)
    }
    
    @IBAction func evaluateScore(_ sender: Any) {
        clearEvaluationScore()
        disableViewReportButton()
        self.viewReport.isEnabled = true
        //Needs to call functions from calculate evaluation score file
        EvaluationScoreCalcualtions().calculateScoreSimulation(selectedParameters: childVC?.selectedEvaluationParameters ?? []) { (evaluationParameterScore, parameterId) in
            //set label text
            self.evaluationScore.setEvaluationScoreForEachParameter(evaluationScore: evaluationParameterScore, evaluationParameterId: parameterId)
            var currentTotalScore = Int(self.scoreLabel.text ?? "0")!
            currentTotalScore += evaluationParameterScore
            self.scoreLabel.text = String(currentTotalScore)
            if(parameterId == 3) {
                //when the last parameterId is completed
                self.enableViewReportButton()
            }
        }
    }
    
    func clearEvaluationScore() {
        evaluationScore = EvaluationScore()
        self.scoreLabel.text = "0"
    }
    
    func enableViewReportButton() {
        self.viewReport.isEnabled = true
    }
    
    func disableViewReportButton() {
        self.viewReport.isEnabled = false
    }
    
    @IBAction func resetScoreCalculations(_ sender: Any) {
        //Set score back to 0
        //Unselect all the sections
        //Enable the view report button once score is calcualted
        
        clearEvaluationScore()
        
        //        self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableViewScrollPosition.none)
        //        self.tableView(self.tableView, didSelectRowAt: indexPath)
        for index in 0...3 {
            let indexPath = IndexPath(row: index, section: 0)
            childVC?.tableView.deselectRow(at: indexPath, animated: true)
            childVC?.removeParameterSelectedParameters(with: indexPath)
        }
        
        disableViewReportButton()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "simulationToReport") {
            let reportVC = segue.destination as! ReportTableViewController
            reportVC.parametersEvalautionScore = evaluationScore.getParemetersEvalautionScore()
        }
    }
    
}
