//
//  HomeViewController.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/13/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//

import UIKit
import Firebase

class HomeViewController: UIViewController {
    
    @IBOutlet weak var userMenu: UIView!
    
    @IBAction func clickUserMenu(_ sender: Any) {
        if(userMenu.isHidden) {
            userMenu.isHidden = false
        } else {
            userMenu.isHidden = true
        }
    }
    
    @IBAction func openUserProfile(_ sender: Any) {
        //TO DO: Need to create Profile View controller and to push it modaly from here
    }
    
    @IBAction func logoutUser(_ sender: Any) {
        try! Auth.auth().signOut()
        
        if let storyboard = self.storyboard {
            let vc = storyboard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    @IBAction func openAboutUs(_ sender: Any) {
        //TO DO: Need to create About Us View controller and to push it modaly from here
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
