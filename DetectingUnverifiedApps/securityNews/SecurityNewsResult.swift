//
//  SecurityNewsResult.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/31/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
//This structs are used for parsing the News Api and get the Articles and information about them


import Foundation

struct SecurityNewsResult {
    let totalResults: Int
    let articles: [NewsArticle]
}

extension SecurityNewsResult {
    init?(json: [String: Any]) {
        guard let totalResults = json["totalResults"] as? Int,
            let articlesDict = json["articles"] as? [Any]
            else {
                return nil
        }
        
        var newsArticles: [NewsArticle] = []
        for newsArticleJson in articlesDict {
            guard let newsArticle = NewsArticle(json: (newsArticleJson as? [String: Any])!)
                else {
                    return nil
            }
            
            newsArticles.append(newsArticle)
        }
        
        self.totalResults = totalResults
        self.articles = newsArticles
    }
}

struct NewsArticle {
    //This struct contains the actual relevant keys which are used to
    //determine if the appsName is valid or not
    
    let title : String //title of the article
    let author: String  //name of the author of the article
    let url : String //URL of the article
    let description: String //description of the article
    let urlToImage: String //image URL associated with article
}

extension NewsArticle {
    init?(json: [String: Any]) {
        guard let title = json["title"] as? String,
            let author = json["author"] as? String,
            let url = json["url"] as? String,
            let description = json["description"] as? String,
            let urlToImage = json["urlToImage"] as? String
            else {
                return nil
        }
        
        self.title = title
        self.author = author
        self.url = url
        self.description = description
        self.urlToImage = urlToImage
    }
}
