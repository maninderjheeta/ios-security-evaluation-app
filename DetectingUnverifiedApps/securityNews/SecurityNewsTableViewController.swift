//
//  SecurityNewsTableViewController.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/25/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//

import UIKit

class SecurityNewsTableViewController: UITableViewController {
    /*
        This is table view controller for the News Screen.
     */

    let IOS_SECURITY_NEWS_QUERY = "iphone+security"
    let API_KEY = "Your API Key"
    var newsArticles: [NewsArticle] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadNewsData(query: self.IOS_SECURITY_NEWS_QUERY)
    }
    
    func loadNewsData(query queryText: String) {
        let semaphore = DispatchSemaphore(value: 0)
        let session = URLSession.shared
        let url = URL(string: "https://newsapi.org/v2/everything?q=\(queryText)&apiKey=\(API_KEY)")!
        let task = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil || data == nil {
                print("Client error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                semaphore.signal()
                return
            }
            
            self.processData(with: data!)
            semaphore.signal()
        }
        
        task.resume()
        _ = semaphore.wait(timeout: DispatchTime.distantFuture)
    }
    
    func processData(with data: Data) -> Void {
        var securityNewsResult: SecurityNewsResult? = nil
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            securityNewsResult = SecurityNewsResult(json: json as! [String : Any])
        } catch {
            print("Error in JSON response: \(error.localizedDescription)")
        }
        
        if(securityNewsResult != nil && securityNewsResult?.totalResults != 0 && securityNewsResult!.articles.count != 0) {
            newsArticles = securityNewsResult!.articles
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.newsArticles.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsArticleCell", for: indexPath)

        // Configure the cell...
        let index = indexPath.row
        cell.textLabel?.text = newsArticles[index].title
        cell.detailTextLabel?.text = newsArticles[index].author

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        let newsArticleUrl = newsArticles[index].url
        guard let url = URL(string: newsArticleUrl) else { return }
        UIApplication.shared.open(url)
    }

}
