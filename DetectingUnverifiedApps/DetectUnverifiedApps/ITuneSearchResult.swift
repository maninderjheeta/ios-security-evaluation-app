//
//  ITuneSearchResult.swift
//  TestAPI
//
//  Created by Maninder Singh Jheeta on 7/12/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
// Credits
// Reference: https://developer.apple.com/swift/blog/?id=37
//

import Foundation

struct ITuneAppSearchResult {
    /*
     This struct is used to parse the JSON response returned from the iTunes api.
     It contains information about various things. We are only parsing the bundleId and
     trackCensoredName key which will help us to check if the app label is contained in any of this two keywords across all the returned values (there is an array of result returned)
     */
    
    let resultCount: Int
    let allAppsInfo: [SearchKeysOptionsForValidation]
    
    func isAppNamePresentInResults(appName name : String) -> Bool {
        var isAppNameValid = false
        for appInfo in allAppsInfo {
            isAppNameValid = appInfo.bundleId.contains(name) || appInfo.trackCensoredName.contains(name)
            
            if(isAppNameValid) {
                return isAppNameValid
            }
        }
        
        return isAppNameValid
    }
}

extension ITuneAppSearchResult {
    init?(json: [String: Any]) {
        guard let resultCount = json["resultCount"] as? Int,
            let results = json["results"] as? [Any]
            else {
                return nil
        }
        
        var allAppsInfo: [SearchKeysOptionsForValidation] = []
        for result in results {
            guard let searchKeysOption = SearchKeysOptionsForValidation(json: (result as? [String: Any])!)
                else {
                    return nil
            }
            
            allAppsInfo.append(searchKeysOption)
        }
        
        self.resultCount = resultCount
        self.allAppsInfo = allAppsInfo
    }
}

struct SearchKeysOptionsForValidation {
    //This struct contains the actual relevant keys which are used to
    //determine if the appsName is valid or not
    
    let bundleId : String //bundleId is the key
    let trackCensoredName : String //trackCensoredName is the key
}

extension SearchKeysOptionsForValidation {
    init?(json: [String: Any]) {
        guard let bundleId = json["bundleId"] as? String,
            let trackCensoredName = json["trackCensoredName"] as? String
            else {
                return nil
        }
        
        self.bundleId = bundleId
        self.trackCensoredName = trackCensoredName
    }
}

