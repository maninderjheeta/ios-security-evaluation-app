//
//  Copyright (c) 2018 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//  The file has been updated to suit the project needs. Credit goes to google fir sharing the example to use Firebase.

//  Created by Maninder Singh Jheeta on 7/31/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//

import FirebaseMLVision
import FirebaseMLModelInterpreter
import FirebaseMLCommon
import UIKit
import BSImagePicker
import Photos


class DetectUnverifiedAppsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDataSource {
    var images : [UIImage] = []
    var appLabels : [String] = []
    var unverifiedApps : [String] = []
    var verifiedApps: [String] = []
    let spinnerAsChild = SpinnerViewController()
    
    @IBOutlet weak var appLabelTableView: UITableView!
    @IBOutlet weak var unverifiedAppTableView: UITableView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    private lazy var resultsAlertController: UIAlertController = {
        let alertController = UIAlertController(title: "Detection Results",
                                                message: nil,
                                                preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "OK", style: .destructive) { _ in
            alertController.dismiss(animated: true, completion: nil)
        })
        return alertController
    }()
    
    private lazy var vision = Vision.vision()
    private lazy var textRecognizer = vision.onDeviceTextRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set the delegate for the tableview
        appLabelTableView.dataSource = self
        unverifiedAppTableView.dataSource = self
    }

    @IBAction func showInstalledApps(_ sender: Any) {
        let actionsSheet = UIAlertController(title: "App Screenshots", message: "Choose apps screenshot taken from Settings Screen", preferredStyle: .actionSheet)
        
        actionsSheet.addAction(UIAlertAction(title: "Photo gallery", style: .default, handler: {(action: UIAlertAction) in
            self.bsImagePicker()
        }))
        
        actionsSheet.addAction(UIAlertAction(title: "Don't have one: Go to Settings", style: .default, handler: {(action: UIAlertAction) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }))

        actionsSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(actionsSheet, animated: true, completion: nil)
    }
    
    func bsImagePicker() {
        let vc = BSImagePickerViewController()
        vc.maxNumberOfSelections = 6
        
        bs_presentImagePickerController(vc, animated: true,
                                        select: { (asset: PHAsset) -> Void in
                                            print("Selected: \(asset)")
        }, deselect: { (asset: PHAsset) -> Void in
            print("Deselected: \(asset)")
        }, cancel: { (assets: [PHAsset]) -> Void in
            print("Cancel: \(assets)")
        }, finish: { (assets: [PHAsset]) -> Void in
            print("Finish: \(assets)")
            self.processAfterImageSelection(assets: assets)
        }, completion: nil)
    }
    
    func getImagesFromPHAssets(assets: [PHAsset]) -> [UIImage] {
        var selectedImages: [UIImage] = []
        for asset in assets {
            selectedImages.append(getAssetThumbnail(asset: asset))
        }
        
        return selectedImages
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        //
        // Credit goes to this stackoverflow answer as it helped in the converting the PHAsset object to UIImae
        //Reference: https://stackoverflow.com/questions/30812057/phasset-to-uiimage
        //
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    func processAfterImageSelection(assets: [PHAsset]) {
        let selectedImages = self.getImagesFromPHAssets(assets: assets)
        
        startSpinner()
        recognizeTextRecognitionForImages(with: selectedImages)
    }
    
    // MARK: Text Recognition
    func recognizeTextRecognitionForImages(with images: [UIImage]) -> Void {
        for image in images {
            let visionImage = VisionImage(image: image)
            textRecognizer.process(visionImage) { features, error in
                self.recognizeLabelsInImage(from: features, error: error)
            }
        }
    }
    
    func recognizeLabelsInImage(from text: VisionText?, error: Error?) -> Void {
        guard error == nil, let text = text else {
            let errorString = error?.localizedDescription ?? Constants.detectionNoResultsMessage
            print("Text recognizer failed with error: \(errorString)")
            stopSpinner()
            return
        }
        
        for block in text.blocks {
            for line in block.lines {
                for element in line.elements {
                    let label = element.text
                    print("This is App label: \(label)")
                    self.appLabels.append(label)  //this is the label text
                }
            }
        }
        
        var verifiedAndUnverifiedAppLabels = AppsValidation.getUnverifiedApps(appNames: self.appLabels)
        self.verifiedApps = verifiedAndUnverifiedAppLabels[0]
        self.unverifiedApps = verifiedAndUnverifiedAppLabels[1]
        
        appLabelTableView.reloadData()
        unverifiedAppTableView.reloadData()
        stopSpinner()
    }
    
    func startSpinner() {
        //start spinner here
        addChild(spinnerAsChild)
        spinnerAsChild.view.frame = view.frame
        view.addSubview(spinnerAsChild.view)
        spinnerAsChild.didMove(toParent: self)
        //spinner started
    }
    
    func stopSpinner() {
        //stopping spinner here
        spinnerAsChild.willMove(toParent: nil)
        spinnerAsChild.view.removeFromSuperview()
        spinnerAsChild.removeFromParent()
        //spinner stoped
    }
    
    //MARK : - code for table view
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == self.appLabelTableView {
            return "Verified Apps"
        } else if tableView == self.unverifiedAppTableView {
            return "Unverified Apps"
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.appLabelTableView {
            return self.verifiedApps.count
        } else if tableView == self.unverifiedAppTableView {
            return self.unverifiedApps.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == appLabelTableView, let cell = tableView.dequeueReusableCell(withIdentifier: "verifiedAppId") {
            let verifiedAppLabel = self.verifiedApps[indexPath.row]
            cell.textLabel?.text = verifiedAppLabel
            return cell
        } else if tableView == unverifiedAppTableView, let cell = tableView.dequeueReusableCell(withIdentifier: "unverifiedAppId") {
            let unverifiedAppLabel = self.unverifiedApps[indexPath.row]
            cell.textLabel?.text = unverifiedAppLabel
            return cell
        }
        
        return UITableViewCell()
    }
}

fileprivate enum Constants {
    static let detectionNoResultsMessage = "No results returned."
}

// MARK : - Spinner Indicator code

class SpinnerViewController: UIViewController {
    /*
     Credit goes to the Hacking Swift website which helped me to code a spinner for my app.
     Reference code: https://www.hackingwithswift.com/example-code/uikit/how-to-use-uiactivityindicatorview-to-show-a-spinner-when-work-is-happening
     */
    var spinner = UIActivityIndicatorView(style: .whiteLarge)
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)
        
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}
