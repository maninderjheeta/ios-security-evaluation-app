//
//  Validation.swift
//  TestAPI
//
//  Created by Maninder Singh Jheeta on 7/12/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
//This class validates if the app label is a verified app or an unverfiied app.
//It perform bunch of cleanups. It calls iTunes API to check if the app name is present in the App store or not.

import Foundation
import Firebase

class AppsValidation {

    static func getUnverifiedApps(appNames names: [String]) -> [[String]] {
        var verifiedAppsName : [String] = []
        var unverifiedAppsName : [String] = []
        
        let uniqueAppNames = removeDuplicateAppNames(appNames: names)
        for name in uniqueAppNames {
            if(isAppNameFromApple(appName: name)) {
                verifiedAppsName.append(AppValidationConstants.APPLE_INBUILT_APPS[name]!)
            } else if(isAppNameKnown(appName: name)) {
                verifiedAppsName.append(AppValidationConstants.VERIFIED_APPS[name]!)
            } else if(isGarbageLabel(appName: name)) {
                continue
            } else if(!isAppNameValid(appName: name)) {
                unverifiedAppsName.append(name)
            } else {
                verifiedAppsName.append(name)
            }
        }
        
        verifiedAppsName = removeDuplicateAppNames(appNames: verifiedAppsName)
        unverifiedAppsName = removeDuplicateAppNames(appNames: unverifiedAppsName)
        
        writeAppsInfoToFirebase(verifiedApps: verifiedAppsName, unverifiedApps: unverifiedAppsName)
        return [verifiedAppsName, unverifiedAppsName]
    }
    
    static func writeAppsInfoToFirebase(verifiedApps: [String], unverifiedApps: [String]) {
        let uidUser = Auth.auth().currentUser!.uid
        let ref = Database.database().reference() //Firebase reference
        
        ref.child(uidUser).child("appLabels").setValue(["unverifiedAppLabels": unverifiedApps, "verifiedAppLabels": verifiedApps])
    }
    
    static func isAppNameFromApple(appName name: String) -> Bool {
        return AppValidationConstants.APPLE_INBUILT_APPS[name] != nil
    }
    
    static func isAppNameKnown(appName name: String) -> Bool {
        return AppValidationConstants.VERIFIED_APPS[name] != nil
    }
    
    static func removeDuplicateAppNames(appNames names: [String]) -> [String] {
        return Array(Set(names))
    }
    
    static func isGarbageLabel(appName name: String) -> Bool {
        //if label has very few character
        if(name.count < 2) {
            return true
        }
        
        if(AppValidationConstants.GARBAGE_LABELS.contains(name)) {
            return true
        }
        
        //check if it is a possible time string 12:00, try all possibilities
        if(isNameATimeString(appName: name)) {
            return true
        }
        
        return false
    }
    
    static func isNameATimeString(appName name: String) -> Bool {
//        if(name.count == 5 && name.contains(":")) {
//            if(DIGITS.contains(name[0]) && DIGITS.contains(name[1]) &&
//                DIGITS.contains(name[3]) && DIGITS.contains(name[4])) {
//                return true
//            }
//        }
        
        if(isNameATimeStringHelper(appName: name)) {
            return true
        }
        
        return false
    }
    
    static func isNameATimeStringHelper(appName name: String) -> Bool {
        for ch in name {
            if(!(AppValidationConstants.DIGITS.contains(ch) || ch == ":")) {
                return false
            }
        }
        
        return true
    }
    
    static func isAppNameValid(appName name: String) -> Bool {
        //Send request to the Apple URL
        //get the JSON response
        //Process the JSON response to check if the app name is present in the
        // bundleID,trackCensoredName
        //other options: is to check if artistViewUrl has the same image as the icon
        //For this we need to get the piece of the image and work on it.
        // return true or false based on the result.
        
        
        //May need to show an alert to the user that there is some probem with the URL
        let semaphore = DispatchSemaphore(value: 0)
        var isValidAppName = false
        let session = URLSession.shared
        let url = URL(string: "https://itunes.apple.com/search?term=\(name)&country=us&entity=software")!
        let task = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil || data == nil {
                print("Client Side Error: issue the request to the iTune API error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error: Error from the iTune API server")
                semaphore.signal()
                return
            }
            isValidAppName = processData(with: data!, appName: name)
            
            semaphore.signal()
        }
        
        task.resume()
        _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        
        return isValidAppName
    }
    
    static func processData(with data: Data, appName name: String) -> Bool {
        var isAppNameValid = false;
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            let ituneAppSearchResult = ITuneAppSearchResult(json: json as! [String : Any])
            
            //means no app with this appName present in App store
            if(ituneAppSearchResult?.resultCount == 0) {
                return false
            }
            
            isAppNameValid = (ituneAppSearchResult?.isAppNamePresentInResults(appName: name))!
        } catch {
            print("Error in JSON response: \(error.localizedDescription)")
        }
        
        return isAppNameValid
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
}

