//
//  AppValidationConstants.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 7/31/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//
//This are constants used for cleaning up the App labels


import Foundation

class AppValidationConstants {
    static let GARBAGE_LABELS = ["Settings", "EE", "", "APP", "YouTuba.Mucin", "BUS", "LTE", "lte", "Lte", "lTe", "LTe", "TV Provider", "RIDE", "PS",
                                 "Run", "Express", "ilndeed", "yALyft", "Club", "in", "NRC", "Info", "LITE", "Lite", "Jobs", "(Merriam-", "DUe", "GRUBHUB",
                                 "Cash", "Bose", "Buy", "Store", "3e", "Mind", "Center", "1ANB", "tv", "Mobile", "Provider", "*****", "*", "**", "***",
                                 "****", "Memos", "WN", "Voice", "ES", "Insight"]
    
    static let APPLE_INBUILT_APPS = ["Mail": "Apple-Mail", "Contacts": "Apple-Contacts", "Calendar": "Apple-Calendar", "Notes": "Apple-Notes",
                                     "Reminders": "Apple-Reminders", "Voice Memos":"Apple-Voice Memos", "Phone": "Apple-Phone", "Messages": "Apple-Messages",
                                     "FaceTime":"Apple-FaceTime", "Maps": "Apple-Maps", "Compass": "Apple-Compass", "Measure":"Apple-Measure",
                                     "Safari": "Apple-Safari", "News":"Apple-News", "Stocks":"Apple-Stocks", "Music":"Apple-Music", "TV":"Apple-TV", 
                                    "Photos":"Apple-Photos", "Camera":"Apple-Camera", "Books":"Apple-Books", "Podcasts":"Apple-Podcasts", "iTunes U":"Apple-iTunes U",
                                    "Game Center":"Apple-Game Center", "Numbers": "Apple-Numbers", "Pages": "Apple-Pages", "iMovies": "Apple-iMovies",
                                    "Clips":"Apple-Clips", "Game": "Apple-Game Center", "iTunes": "Apple-iTunes U", "iMovie": "Apple-iMovies"]
    
    static let DIGITS = "0123456789"
    static let VERIFIED_APPS = ["Linkedin": "LinkedIn", "Lirum": "Lirum Info Lite", "iIndeed": "Indeed Jobs", "Duo": "Duo Mobile",
                                "Webster": "Merriam Webster Dictionamry", "Giant": "1 Gaint Mind", "Aetna": "Aetna Mobile", "Cash":
        "Cash App", "Bose": "Bose Hear", "Citi": "Citi Mobile", "GRUBHUB": "Grubhub", "Insight": "Insight Timer"]
}
