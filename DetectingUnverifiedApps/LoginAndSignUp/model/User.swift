//This is the User struct to manage the user login and sign up
//this file is Copyright (c) 2018 Razeware LLC
//Credit goes to Razeware LLC for this file

import Foundation
import Firebase

struct User {
  
  let uid: String
  let email: String
  
  init(authData: Firebase.User) {
    uid = authData.uid
    email = authData.email!
  }
  
  init(uid: String, email: String) {
    self.uid = uid
    self.email = email
  }
}
