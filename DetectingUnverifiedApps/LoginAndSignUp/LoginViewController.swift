/*
 
 
 Credit goes to Ray Wenderlich. Link is provided. The file is updated after taking the code from them.
 It may not be similar what has been provided in the link.
 Reference https://www.raywenderlich.com/3-firebase-tutorial-getting-started
 
 @author: Maninder Singh Jheeta
 
 This is a UIViewController for managing the login for the users
 */

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    let loginToHomeScreen = "LoginToHomeScreen"
    
    var user: User?
    
    @IBOutlet weak var textFieldLoginEmail: UITextField!
    @IBOutlet weak var textFieldLoginPassword: UITextField!
    @IBOutlet weak var errorMessage: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //Check if user is already signed in
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                self.performSegue(withIdentifier: self.loginToHomeScreen, sender: nil)
                self.textFieldLoginEmail.text = nil
                self.textFieldLoginPassword.text = nil
            }
        }
    }
    
    //Action on click of login button
    @IBAction func loginDidTouch(_ sender: AnyObject) {
        //Entering to perform login
        
        guard let email = textFieldLoginEmail.text,
            let password = textFieldLoginPassword.text
            else {
                return
        }
        
        if(email.count <= 0 || password.count <= 0) {
            return
        }
        
        //passed email & password checks

        Auth.auth().signIn(withEmail: email, password: password) { user, error in
            if let error = error, user == nil {
                let alert = UIAlertController(title: "Sign In Failed",
                                              message: error.localizedDescription,
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        //logged in successfully
        
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                self.performSegue(withIdentifier: self.loginToHomeScreen, sender: nil)  //Segiue to the Home Screen
                self.textFieldLoginEmail.text = nil
                self.textFieldLoginPassword.text = nil
            }
        }
    }

}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldLoginEmail {
            textFieldLoginPassword.becomeFirstResponder()
        }
        if textField == textFieldLoginPassword {
            textField.resignFirstResponder()
        }
        return true
    }
}
