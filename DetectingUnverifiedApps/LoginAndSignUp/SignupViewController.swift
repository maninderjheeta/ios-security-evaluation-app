//
//  SignupViewController.swift
//
//  Created by Maninder Singh Jheeta on 6/23/19.
//
//
//Credit goes to Ray Wenderlich. Link is provided. The file is updated after taking the code from them.
//It may not be similar what has been provided in the link.
//
//  Reference https://www.raywenderlich.com/3-firebase-tutorial-getting-started
//  Copyright © 2019 Razeware LLC. All rights reserved.
//

import UIKit
import Firebase

class SignupViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var retypePasswordTextField: UITextField!
    
    @IBOutlet weak var errorMessage: UILabel!
    
    @IBAction func didTouchSignup(_ sender: Any) {
        
        let email = emailTextField.text != nil ? emailTextField.text! : ""
        let password = passwordTextField.text != nil ? passwordTextField.text! : ""
        let retypePassword = retypePasswordTextField.text != nil ? retypePasswordTextField.text! : ""
        
        if(!validateFields(email: email, password: password, retypePassword: retypePassword)) {
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { user, error in
            if error == nil {
                print("Hope fully created user")
                Auth.auth().signIn(withEmail: email,
                                   password: password)
            } else {
                self.errorMessage.text = "Sign up error"
                return
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTouchLogin(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //Reference: https://stackoverflow.com/questions/25471114/how-to-validate-an-e-mail-address-in-swift
    //check is email is valid or not
    //returns true if email is valid
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: email)
    }
    
    //Reference: https://stackoverflow.com/questions/39284607/how-to-implement-a-regex-for-password-validation-in-swift
    //check is password is valid or not
    //returns true if password is valid
    //Regex checks if Password has more than 6 characters, has at least one capital, has a numeric or special character"
    func isValidPassword(password:String) -> Bool {
        return true
//        let passwordRegEx = "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$"
//        
//        let passwordPredicate = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
//        return passwordPredicate.evaluate(with: password)
    }
    
    func validateFields(email:String, password:String, retypePassword:String) -> Bool {
        if(email.count < 0) {
            self.errorMessage.text = "Email is empty"
            return false
        }
        
        if(password.count < 0) {
            self.errorMessage.text = "Password is empty"
            return false
        }
        
        if(retypePassword.count < 0) {
            self.errorMessage.text = "Retype password is empty"
            return false
        }
        
        if(!isValidEmail(email: email)) {
            self.errorMessage.text = "Email is invalid"
            return false
        }
        
        
        if(!isValidPassword(password: password)) {
            self.errorMessage.numberOfLines = 0
            self.errorMessage.text = """
            Password:
            - must be more than 6 characters
            - contains at least one capital
            - contains a numeric or special character
            """
            return false
        }
        
        if(password != retypePassword) {
            self.errorMessage.text = "Password and retype password are not same"
            return false
        }
        
        return true
    }//validateFields

}
