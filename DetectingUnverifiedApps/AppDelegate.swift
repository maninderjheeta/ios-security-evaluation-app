//
//  AppDelegate.swift
//  DetectingUnverifiedApps
//
//  Created by Maninder Singh Jheeta on 6/25/19.
//  Copyright © 2019 Maninder Singh Jheeta. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Fire base configuration
        FirebaseApp.configure()
        //to work offline as well
        Database.database().isPersistenceEnabled = true
        
        return true
    }

}

